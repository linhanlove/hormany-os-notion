# HormanyOs-notion

#### 🍃介绍🍃
鸿蒙版《概念笔记》 将完成web端notion到hormanyOs的适配

#### 迁移
本项目已完成对于（API14）的迁移，详情查看[使用5.0.2(API14)完成对之前hormany_os_notion的迁移](https://gitee.com/linhanlove/hormany-os_atomic)

#### ✍软件架构✍
hormanyOs4.0
本项目基于 API9,如果发现，项目拉取到本地无法运行，原因大多是因为编辑器版本与API版本不兼容，可降低编辑器版本尝试
另外，本人基于最新的API13开发了一个鸿蒙工具库：
- harmonyos-utils 是基于harmony_Os最新的api(API13)工具库，旨在提供项目开发中不可或缺的实用工具函数。通过简单的导入语句，您可以快速地将这些工具函数集成到您的项目中，无需任何复杂配置。
- 项目地址：https://gitee.com/linhanlove/harmonyos-utils
- 欢迎有此类工具的同学将工具函数集成进来，方便后续使用

#### 目录结构
```
├──entry/src/main/ets	                 // 代码区
│  ├──common                             // 公共文件夹
│  │  ├──Constants                       // 公共常量类
│  │  ├──mock                            // 模拟数据
│  │  ├──utils                           // 工具函数
│  │  └──StyleConstants                  // 公共样式常量类
│  ├──CommonComponents                   // 公共组件
│  │  └──BaseComponents.ets              // 全局基础组件
│  ├──entryability
│  │  └──EntryAbility.ets	            // 程序入口类
│  ├──features                          // 功能组件文件夹 以tabBar页面分类
│  │  └──home	                        // 以tabBar页面分类
│  ├──pages                             // tabBar主入口页面
│  │  ├──article.ets                    // 文章
│  │  ├──auth.ets                       // 登录注册
│  │  ├──community.ets                  // 社交
│  │  ├──home.ets                       // 应用首页
│  │  └──mina.ets                       // 我的
│  └──view                              // 主程序入口文件包含tabBar,启动页
│  │  ├──MainPage.ets                   // tabBar
│  │  ├──SplashPage.ets                 // 启动页
└──entry/src/main/resources	         // 资源文件目录
```
### 🚀参与贡献🚀
1. Fork 本仓库

2. 欢迎有意者加入开发团队，本人前端，技术热爱狂，希望可以学到更多东西


### 🚀特别提示🚀
1.想找一个类似掘金app端的设计原型图，如果有，私我

2.**本人微信：linhan_0119**

3.再次欢迎各位大佬斧正

## 题外话
本人有一个针对前端的工具库，希望有志同道合的，觉得项目对你有用的，一起加入开源
这里是地址 [atom-tools](https://github.com/LinHanlove/atom-tools)


## ✨star✨
拉取代码，如果觉得写的还可以，希望留下您的star✨✨✨✨

## 现有的一些效果图(作者找虐,每天赶进度!!! 欢迎斧正 🙊)

## 加载页
![img.png](img.png)

## 登录页
![img_1.png](img_1.png)

## 注册页
![img_2.png](img_2.png)

## tabBar切换
![img_3.png](img_3.png)